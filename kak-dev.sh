#!/usr/bin/env bash
set -euo pipefail

#Google drive
curl -L 'https://github.com/gdrive-org/gdrive/releases/download/2.1.0/gdrive-linux-x64' -o /usr/local/bin/gdrive
chmod +x /usr/local/bin/gdrive

#Fuzzy search
test -d /usr/local/fzf && cd /usr/local/fzf && git reset --hard master && cd - || git clone --depth 1 https://github.com/junegunn/fzf.git /usr/local/fzf
cat >/etc/profile.d/fzf.sh <<EOF
export XDG_CONFIG_HOME=\$HOME/.config
test -d ~/.config/fzf || /usr/local/fzf/install --all --xdg
export PATH=\$PATH:/usr/local/fzf/bin/
EOF

#Terminal multiplexer
cat >/etc/tmux.conf <<EOF
set -sg escape-time 25
set -g mouse on
set -g status off
EOF

#Go and file browser
cat >/etc/profile.d/go.sh <<EOF
export GOPATH=/usr/local/go
export PATH=\$GOPATH/bin:\$PATH
EOF
. /etc/profile.d/go.sh
mkdir -p $GOPATH

go get -u github.com/gokcehan/lf
mkdir -p /etc/lf
cat >/etc/lf/lfrc <<EOF
cmd rename %mv -i \$f \$1
map r push :rename<space>
set hidden
EOF

#Grep
curl -fsL https://github.com/BurntSushi/ripgrep/releases/download/11.0.1/ripgrep-11.0.1-x86_64-unknown-linux-musl.tar.gz | tar -zx ripgrep-11.0.1-x86_64-unknown-linux-musl/rg
mv ripgrep-11.0.1-x86_64-unknown-linux-musl/rg /usr/local/bin/
rm -Rf ripgrep-11.0.1-x86_64-unknown-linux-musl


#Kakoune
npm install -g vscode-{html,css,json}-languageserver-bin bash-language-server vue-language-server

mkdir -p /root/.config/kak/plugins/
cat >/root/.config/kak/kakrc <<EOF
source "%val{config}/plugins/plug.kak/rc/plug.kak"
plug "andeyost/plug.kak" noload config %{
    set-option global plug_always_ensure true
}

plug "TeddyDD/kakoune-lf"

plug "andreyorst/fzf.kak" %{
    map global normal <c-p> ': fzf-mode<ret>'
    set-option global fzf_file_command "find . \( -path '*/.svn*' -o -path '*/.git*' \) -prune -o -type f -print"
}

plug "occivink/kakoune-find"

plug "andreyorst/powerline.kak"

add-highlighter global/ number-lines -relative -hlcursor
add-highlighter global/ show-matching
set-face global PrimaryCursor black,yellow
set-face global MatchingChar black,white

set global grepcmd 'rg --column -i'

plug "ul/kak-lsp" do %{
    cargo build --release --locked
    cargo install --force --path .
} config %{
    lsp-enable
}

plug "andreyorst/smarttab.kak" %{
  set-option global softtabstop 2
  set-option global indentwidth 2
  set-option global tabstop 2
  hook global WinSetOption expandtab
  hook global WinSetOption filetype=(makefile|gas) noexpandtab
  hook global WinSetOption filetype=(c|cpp) smarttab
}

set global scrolloff 2,2
EOF

test -d /root/.config/kak/plugins/plug.kak && cd /root/.config/kak/plugins/plug.kak && git pull || git clone https://github.com/andreyorst/plug.kak.git /root/.config/kak/plugins/plug.kak

test -d /usr/local/src/kakoune || git clone https://github.com/mawww/kakoune.git /usr/local/src/kakoune
cd /usr/local/src/kakoune
git fetch && git reset --hard origin/master
cd src
make
make install

echo "OK"
