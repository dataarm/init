#!/usr/bin/env bash
set -euo pipefail 

#Xtra packages
apt-get install -y libncursesw5-dev pkg-config npm tmux build-essential golang cargo mosh whois unzip zip

npm install -g yarn

apt-get install -y python3-pip python3-setuptools libpython3-dev

#AWS
pip3 install --upgrade awscli aws-sam-cli

#Azure
wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb
apt-get update
add-apt-repository universe
apt-get install -y powershell

echo "OK"
