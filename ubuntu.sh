#!/usr/bin/env bash
set -euo pipefail 

#Disable firewall
systemctl stop apparmor || true
systemctl disable apparmor || true
apt-get remove -y ufw apparmor

#Email
systemctl stop postfix || true
systemctl disable postfix || true
apt-get remove postfix -y

#Xtra packages
apt-get install -y git wget glances s-nail jq
apt-get update -y

#Docker
apt-get install -y docker.io docker-compose gnupg2 pass

echo "OK"
