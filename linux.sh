#!/usr/bin/env bash
set -euo pipefail

#Disable firewall
iptables -F
iptables -X
iptables --policy FORWARD ACCEPT

#Git & repos
cat >/etc/profile.d/git.sh <<EOF
git config --global credential.helper 'cache --timeout=604800'
EOF

#Docker (after iptables flush)
systemctl restart docker

#Terminal multiplexer
cat >/etc/tmux.conf <<EOF
set -sg escape-time 25
set -g mouse on
EOF

#Limits
sysctl -w vm.max_map_count=262144
echo "vm.max_map_count=262144" >> /etc/sysctl.conf

echo "OK"
