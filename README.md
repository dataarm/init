# KG Init

These are the instructions to initialise infrastructure to the point that we can use it to deploy a Docker-based proof-of-concept (PoC) solution.

Also (working) sample scripts to automate the process.

# Instructions

## Docker

We can deploy to a mix of physical, virtual or container-based infrastructure,
and every component is ready to be scaled up using a variety of high-availability and clustering techniques.

For a PoC engagement, we need to have all components co-existing together within the same/single server,
hence the requirement for initial Docker-based deployment to:

1. Speed-up setup, discovery and
2. Avoid problems with conflicting dependencies, pre-installed software, network addressing and firewall rules.

General instructions are here: https://docs.docker.com/install/, and we recommend either CentOS or Ubuntu for a PoC exercise.

It is not necessary to use the "enterprise" version (EE) of Docker, the "community edition" (CE) is enough as we only need the  "docker-engine" component.

Make sure to have a recent version (18.02+), you can check with:
```
docker info | grep -i version
Server Version: 18.09.6
```

## Docker compose

Should the long-term project opt-in to go with Docker-based deployment in production,
we can use any orchestration mechanism like Docker Swarm, Kubernetes (K8), etc.

For a PoC engagement, though, we'll use Docker compose as it is the easiest to manage.

General instructions are here: https://docs.docker.com/compose/install/

Make sure to have a recent version (1.18+) compatible with the Docker version installed in the previous step, you can check with:
```
docker-compose -v
docker-compose version 1.18.0, build 8dd22a9
```

## Internet

Well, it makes life much easier to have unrestricted access to the internet from the PoC server to download system packages and deploy our software.

If not possible due to security restrictions, we can transfer everything using a USB drive or via the local network,

Please make sure we have an available method to transfer files from our laptop to the PoC server of up to ~3G each (the total size is under ~8G).

## Firewalls and network proxies

Another essential requirement is unfiltered access from your browser to the PoC server on port 80/TCP and 7687/TCP.
We can re-map these ports to any other number (e.g. "8888/TCP" instead of "80/TCP"), but they must not be filtered or proxied.

The overall solution is capable of running behind proxies, firewalls, VPNs and any other enterprise security layer,
but during a PoC we are limited on time and would rather avoid spending it troubleshooting network access problems.

## Admin access

Finally, we need a way to run commands and change settings on the PoC server itself; SSH access with root credentials is a must-have. 

Do note that our solution does not require elevated/administrative privileges to function correctly, but during a PoC engagement time is limited,
so it is much easier if we can help during the setup phase by executing any necessary activity like:

- Modifying "sysctl" settings
- Mounting volumes
- Installing missing packages
- Opening firewall rules
- Configuring security software
- etc.

## Setup
This setup assumes a single-instance for demo/development behind an external firewall
```
apt-get install -y curl bash
curl https://gitlab.com/dataarm/init/raw/master/ubuntu.sh?inline=false | bash -x
curl https://gitlab.com/dataarm/init/raw/master/ubuntu-dev.sh?inline=false | bash -x
curl https://gitlab.com/dataarm/init/raw/master/linux.sh?inline=false | bash -x
curl https://gitlab.com/dataarm/init/raw/master/kak-dev.sh?inline=false | bash -x
```

## Follow-up
Rest of the instructions are on the "dev" or "prod" repo
